<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Задачи</title>
    <link rel="stylesheet" href="https://test.skarlupka.ru/css/bootstrap.min.css">
    <script src="https://test.skarlupka.ru/js/bootstrap.min.js"></script>
</head>
<body>
	<?php include 'app/views/'.$content_view; ?>
</body>
</html>